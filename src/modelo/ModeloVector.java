/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashSet;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloVector implements Modelo {

    Alumno alumnos[] = new Alumno[100];
    Alumno vacio = new Alumno("", "", 0, "");
    int id = 0;

    public ModeloVector() {
        for (int i = 0; i < alumnos.length; i++) {
            alumnos[i] = vacio;
        }
    }

    @Override
    public void create(Alumno alumno) {
        String ids = Integer.toString(id + 1);
        alumno.setId(ids);
        alumnos[id] = alumno;
        id++;

    }

    @Override
    public HashSet read() {
        HashSet hs = new HashSet();
        int i = 0;
        for (i = 0; i < id; i++) {
            if (!alumnos[i].getId().equals("")) {
                hs.add(alumnos[i]);
            }
        }
        return hs;

    }

    public void update(Alumno alumno) {

        int i = 0;
        while (i < alumnos.length) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = alumno;
            }
            i++;
        }

    }

    @Override
    public void delete(Alumno alumno) {
        int i = 0;
        while (i < alumnos.length) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = vacio;
            }
            i++;
        }
    }

}
