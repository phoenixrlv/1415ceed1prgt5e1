/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloArrayList implements Modelo {

    ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
    int id = 0;

    @Override
    public void create(Alumno alumno) {
        String ids = Integer.toString(id + 1);
        alumno.setId(ids);
        alumnos.add(alumno);
        id++;
    }

    @Override
    public HashSet read() {
        HashSet hs = new HashSet();
        Iterator it = alumnos.iterator();
        Alumno a;

        while (it.hasNext()) {
            a = (Alumno) it.next();
            hs.add(a);
        }
        return hs;
    }

    public void update(Alumno alumno) {

        Iterator it = alumnos.iterator();
        Alumno a;
        int pos = 0;

        while (it.hasNext()) {
            a = (Alumno) it.next();
            if (a.getId().equals(alumno.getId())) {
                alumnos.set(pos, alumno);
            }
        }

    }

    @Override
    public void delete(Alumno alumno) {

        Iterator it = alumnos.iterator();
        Alumno a;
        int pos = 0;

        while (it.hasNext()) {
            a = (Alumno) it.next();
            if (a.getId().equals(alumno.getId())) {
                alumnos.remove(a);
            }
            pos++;
        }
    }

}
